package com.example.group15_lab2.DataClasses

data class LocationPosition(
    var locality:String? = null,
    var latitude:Double = 0.0,
    var longitude:Double = 0.0
)