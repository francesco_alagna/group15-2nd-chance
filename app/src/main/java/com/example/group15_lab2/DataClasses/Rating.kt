package com.example.group15_lab2.DataClasses

data class Rating (
    var buyer:String? = null,
    var buyer_nickname:String? = null,
    var rating:Float = 0F,
    var date:String? = null,
    var comment:String? = null
)