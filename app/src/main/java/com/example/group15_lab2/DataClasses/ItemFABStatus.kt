package com.example.group15_lab2.DataClasses

data class ItemFABStatus (
    var isInterested:Boolean = false,
    var soldToMe:Boolean = false
)