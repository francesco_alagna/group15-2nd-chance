package com.example.group15_lab2.ItemList

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.group15_lab2.DataClasses.Item
import com.example.group15_lab2.R
import com.squareup.picasso.Picasso


class MyItemsAdapter: RecyclerView.Adapter<MyItemsAdapter.ViewHolder>() {

    private var items = mutableListOf<Item>()
    private val size : MutableLiveData<Int> by lazy { MutableLiveData<Int>().apply { value=items.size }}
    private lateinit var clickListener: ClickListener

    fun setItemsList(newList:List<Item>){
        items.clear()
        items.addAll(newList)
        size.value = items.size
        notifyDataSetChanged()
    }

    interface ClickListener {
        fun onItemClick(itemID:String?)
        fun onItemEdit(itemID:String?)
    }

    fun setOnItemClickListener(listener: ClickListener) {
        clickListener = listener
    }

    override fun getItemCount() = size.value ?: 0
    fun getSize() : LiveData<Int> = size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.cardview_item,parent,false)
        return ViewHolder(v,clickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ViewHolder(v: View, listener: ClickListener) : RecyclerView.ViewHolder(v) {
        private val bn_edit: ImageView

        init {
            v.setOnClickListener {
                if(adapterPosition != RecyclerView.NO_POSITION){
                    listener.onItemClick(items[adapterPosition].ID)
                }
            }
            bn_edit =  v.findViewById(R.id.cardview_bn_edit)
            bn_edit.setOnClickListener {
                if(adapterPosition != RecyclerView.NO_POSITION){
                    listener.onItemEdit(items[adapterPosition].ID)
                }
            }

        }
        private val name: TextView = v.findViewById(R.id.cardview_name)
        private val price: TextView = v.findViewById(R.id.cardview_price)
        private val expire_date: TextView = v.findViewById(R.id.cardview_date)
        private val image: ImageView = v.findViewById(R.id.cardview_image)
        private val currency:TextView = v.findViewById(R.id.cardview_currency)
        private val category:TextView = v.findViewById(R.id.cardview_category)
        private val status:TextView = v.findViewById(R.id.cardview_status)
        private val card:View =v.findViewById(R.id.cardview)
        private val location:TextView = v.findViewById(R.id.cardview_location)

        fun bind(item: Item){
            name.text = item.title
            price.text = item.price
            expire_date.text = item.expireDate
            currency.text = item.currency
            category.text = item.category
            status.text = item.status
            location.text = item.location

            Picasso.get()
                .load(item.imageURL.toUri())
                .fit()
                .centerInside()
                .error(R.drawable.item_icon)
                .into(image)

            val color =
                when (item.status) {
                    "Sold" -> Color.parseColor("#FFCDD2") //Red
                    "No longer on sale" -> Color.parseColor("#F5F5F5") //Gray
                    else -> Color.parseColor("#BBDEFB") //Blue
                }
            card.setBackgroundColor(color)

        }
    }

}